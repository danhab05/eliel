import os
from re import U
from tkinter import filedialog
from flask import Flask, request
from flask_cors import CORS
import urllib.request
import openpyxl
import requests as req
import ovh
from oauth2client.service_account import ServiceAccountCredentials
import flet as ft
import random

v1 = "Sms"
os.chdir(os.path.expanduser("~/Documents"))
v2 = str(os.getcwd())
v3 = ""
pathName = ""
loading = False


def main(page: ft.Page):
    global v1, v2, v3, pathName, loading

    # def getPath(e):
    #     global pathName, v1
    #     pathName = filedialog.askopenfilename()
    #     v1 = pathName
    #     ts()

    def select_file(e):
        page.add(filepicker)
        filepicker.pick_files("Select file...")

    # 3) CREATE THE FUNCTION OF EVENT
    def return_file(e):
        global v1, pathName
        file_path.value = e.files[0].path
        pathName = file_path.value
        v1 = pathName
        ts()

    row_filepicker = ft.Row(vertical_alignment="center")
    file_path = ft.Text(value="Selected file path", expand=1)
    # 1) CREATE A FILEPICKER:
    filepicker = ft.FilePicker(on_result=return_file)

    def test(e):
        sms(e, 1)

    def exemple(e):
        sms(e, 2)

    def sms(e, a=0):
        global v1, v2, v3, loading

        loading = True
        v1, v2, v3 = pathName, "", "Chargement..."
        ts()
        client = ovh.Client(
            endpoint="ovh-eu",
            application_key="f345228361555eda",
            application_secret="7843c559598fed4ee362613109898c73",
            consumer_key="0ef49d100212d32b7c7629ae6db144ed",
        )
        name = 0
        firstName = 1
        phoneNumber = 2
        email = 3
        # pathName = ""
        # file_picker = ft.FilePicker(on_result=(lambda e: pathName.set(e)))
        # pathName = "D:/Windows/Downloads/EXCEL finaqsd.xlsx"
        wookbook = openpyxl.load_workbook(pathName)
        worksheet = wookbook.active

        msg = open(os.getcwd() + "/txt.txt", "r", encoding="utf-8").read()

        cn = 0
        save = 1
        end = worksheet.max_row if a == 0 else 2
        for col in worksheet.iter_rows(2, end):
            # v3 = (
            #     str(col[name].value)
            #     + " "
            #     + str(col[firstName].value)
            #     + " "
            #     + str(col[phoneNumber].value)
            #     + " "
            #     + str(col[email].value)
            # )
            if not all([cell.value == None for cell in col]):
                try:
                    newMsg = msg.replace("[prenom]", str(col[name].value))
                    # .replace("[Telephone]", str(col[phoneNumber].value)).replace(
                    #     "[email]", str(col[email].value)).replace("[prenom]", str(col[firstName].value))
                    number = str(col[phoneNumber].value)
                    if "None" in str(number):
                        cn += 1
                        if cn > 5:
                            break
                        else:
                            continue
                    number = number.replace(" ", "")
                    if str(number[:2]) == "33":
                        number = "+" + number
                    elif "+" not in number:
                        number = "+33" + number
                    number = number.replace("+", "")
                    number = int(float(number))
                    number = "+" + str(number)

                    try:
                        v3 = number
                        v2 = newMsg
                        ts()
                        if a != 2:
                            pass
                            result = client.post(
                                "/sms/sms-hd177669-1/jobs/",
                                charset="UTF-8",
                                coding="7bit",
                                message=newMsg,
                                noStopClause=False,
                                priority="high",
                                receivers=[str(number)],
                                senderForResponse=True,
                                validityPeriod=2880,
                                sender="snowy.ovh",
                            )

                        dataL = []
                        dataL += [
                            str(col[name].value),
                            str(col[firstName].value),
                            str(number),
                            str(col[email].value),
                        ]
                        # worksheet.cell(col[name].row, 6).value = "Envoyé"
                        # if save % 10 == 0:
                        #     wookbook.save(pathName)

                    except Exception as e:
                        print(e)
                        pass
                except Exception as e:
                    print(e)
        if a != 2:
            v1, v2, v3 = "Envoyé", "", ""
            ts()

    def ts():
        v1t.value = v1
        v2t.value = v2
        v3t.value = v3
        page.update()

    page.title = "Eliel Sms"
    page.vertical_alignment = ft.MainAxisAlignment.CENTER

    # txt_number = ft.TextField(value="", text_align=ft.TextAlign.RIGHT, width=100)
    v1t = ft.Text(
        value=v1,
        text_align=ft.TextAlign.RIGHT,
    )
    v2t = ft.Text(
        value=v2,
        text_align=ft.TextAlign.RIGHT,
    )
    v3t = ft.Text(
        value=v3,
        text_align=ft.TextAlign.RIGHT,
    )

    page.add(
        ft.Column(
            alignment=ft.MainAxisAlignment.CENTER,
            controls=[
                ft.Row(
                    [
                        ft.ElevatedButton(
                            text="Choisir le excel", on_click=select_file
                        ),
                    ],
                    alignment=ft.MainAxisAlignment.SPACE_BETWEEN,
                ),
                v1t,
                v2t,
                v3t,
                ft.ElevatedButton(text="Exemple sans envoi de sms", on_click=exemple),
                ft.ElevatedButton(
                    text="Faire un test (deuxieme ligne du tableau)", on_click=test
                ),
                ft.ElevatedButton(text="Lancer tout les sms", on_click=sms),
                ft.ProgressRing(visible=loading),
            ],
        )
    )


ft.app(main)


# def mainsms():
#     while True:
#         print("1: SMS")
#         print("2: TEST")
#         print("3: Quitter")
#         choice = input("Choix: ")
#         if choice == "1":
#             sms()
#         elif choice == "2":
#             test()
#         elif choice == "3":
#             break
#         else:
#             print("Invalid choice")
#             continue
